# DOC TEMPLATE - SPHINX

Para usar esse template em seu projeto siga os seguintes passos:

1. Copie a pasta `/docs` para a raiz do projeto
2. Edite as variáveis e links no arquivo de configuração `/docs/source/conf.py`
    de acordo com o projeto
3. Escreva sua documentação em arquivos **markdown** (`.md`) ou 
    **reStructuredText** (`.rst`) em `/docs/source/`
4. Para executar localmente, instale as dependências em seu ambiente virtual 
    rodando, por exemplo: `pip install -r docs/requirements.txt`  
    4.1. Execute o script `./.build_docs.sh` dentro do diretório `/docs`  
    4.2. O HTML gerado pode ser conferido em `/docs/build/html/index.html`  
    4.3. Se tudo estiver ok, limpe o diretório *build* criado com `make clean`
5. Copie o arquivo `.gitlab-ci.yml` para a raiz do seu projeto ou adicione seu 
    conteúdo ao arquivo já existente
6. Done! A cada **commit** realizado na *branch* configurada em `.gitlab-ci.yml`
    sua documentação será atualizada
7. Confira a documentação criada na página do projeto (`Settings -> Pages`).
    Ela provavelmente estará sob o domínio **http://sta.spacetimedocs.com/nome-projeto**.