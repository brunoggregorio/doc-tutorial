"""Root package info."""

__version__ = '0.0.1'
__author__ = 'SpaceTime Labs'
__author_email__ = 'email@spacetimeanalytics.com'
__license__ = 'STL'
__copyright__ = ' 2014-2020, %s.' % __author__
__homepage__ = 'https://spacetimeanalytics.gitlab.io/doc-tutorial'
# this has to be simple string, see: https://github.com/pypa/twine/issues/522
__docs__ = "Doc-Tutorial é um projeto criado para ajudar na documentação dos projetos da STL."
__long_docs__ = """
Doc-Tutorial é um projeto criado para ajudar na documentação dos projetos da STL.
"""

# for compatibility with namespace packages
__import__('pkg_resources').declare_namespace(__name__)
